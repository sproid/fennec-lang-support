# FenneC Syntax Support

A simple extension to provide some syntax coloring for the FenneC programming language. A language used to teach compiler construction at Vrije Universiteit Amsterdam.

## Features

Provides simple syntax highlighting for the FenneC programming language.

## Feature list

- Basic syntax highlighting **X**

## Known Issues

-

## Release Notes

### 0.1.1

- Basic syntax highlighting

-----------------------------------------------------------------------------------------------------------